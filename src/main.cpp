#include <iostream>
#include <vector>

#include "sample.hpp"


int main(int argc, char** argv)
{
    // Simple cmd line program for testing
    CSample<int> intSample;
    int _inp;

    std::cout << "Enter integer numbers. Enter Zero (0) to compute the median and exit." << std::endl;
    std::cin >> _inp;
    while (_inp)
    {
        intSample.AddElement(_inp);
        std::cin >> _inp;
    }

    std::cout << "Median: " << intSample.GetMedian() << std::endl;

    return 0;
}
