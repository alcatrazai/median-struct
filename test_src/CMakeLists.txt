add_executable(run_tests run_tests.cpp)
target_link_libraries(run_tests cppunit)

ADD_CUSTOM_COMMAND(TARGET run_tests POST_BUILD COMMAND "run_tests")
