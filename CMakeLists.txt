cmake_minimum_required(VERSION 3.0)
project(median_struct)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/")
include(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG("-std=c++1z" COMPILER_SUPPORTS_CXX17)
CHECK_CXX_COMPILER_FLAG("-std=c++1y" COMPILER_SUPPORTS_CXX14)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)

set(CMAKE_CXX_FLAGS_DEBUG "-Wall -O0 -g")
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -O3")

set(CMAKE_BUILD_TYPE "Release")

if(COMPILER_SUPPORTS_CXX17)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z")
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has C++17 support. C++17 enabled")
elseif(COMPILER_SUPPORTS_CXX14)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has C++14 support. C++14 enabled")
elseif(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has C++11 support. C++11 enabled")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++ 17/14/11 support. Please use a different C++ compiler.")
endif()

FIND_PACKAGE(CPPUNIT REQUIRED)

set(PROJECT_SOURCES
    src/main.cpp
)

include_directories(${CMAKE_SOURCE_DIR}/include)
FILE(GLOB_RECURSE LibFiles "include/*.hpp")
add_custom_target(headers SOURCES ${LibFiles})

add_subdirectory(test_src)

add_executable(median_struct ${PROJECT_SOURCES})
