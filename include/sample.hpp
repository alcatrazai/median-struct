#ifndef SAMPLE_HPP
#define SAMPLE_HPP

#include <queue>


template<typename T> class CSample      // Make the class template so we can use it for char, int, float, double etc.
{
public:
    CSample():
        _median(0)
    {

    }

    ~CSample()
    {

    }

    void AddElement(T e)
    {
        if (e <= _median)
        {
            if (_left.size() <= _right.size())
            {
                _left.push(e);
            }
            else
            {
                _right.push(_left.top());
                _left.pop();
                _left.push(e);
            }
        }
        else
        {
            if (_right.size() <= _left.size())
            {
                _right.push(e);
            }
            else
            {
                _left.push(_right.top());
                _right.pop();
                _right.push(e);
            }
        }

        // Compute new median
        if (_left.size() < _right.size())
        {
            _median = _right.top();
            return;
        }

        if (_left.size() > _right.size())
        {
            _median = _left.top();
            return;
        }

        // Both PQs are with equal size => get the average of the tops
        _median = (_left.top() + _right.top()) / 2.0;
    }

    double GetMedian() const
    {
        return _median;
    }

    using TMaxPQ = std::priority_queue<T>;
    using TMinPQ = std::priority_queue<T, std::vector<T>, std::greater<typename std::vector<T>::value_type>>;

protected:
    double _median; // Current median - should be floating point even for a sample of integer numbers
    TMaxPQ _left;   // Max PQ for the elements less than current median
    TMinPQ _right;  // Min PQ for the elements greater than the current median
};

#endif // SAMPLE_HPP
